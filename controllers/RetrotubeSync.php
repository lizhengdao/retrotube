<?php
/**
 * Retrotube V1.3
 * Copyright (c) 2020 eric.yape@gmail.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ------------------------------------------------------------------------------
 * @author    eric.yape@gmail.com
 * @license   GNU GENERAL PUBLIC LICENSE Version 3
 * @link      https://gitlab.com/eric.yape/retrotube
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class RetrotubeSync extends CI_Controller {

	public function __construct() {

        parent::__construct();
        //
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        //
        date_default_timezone_set(RT_TIMEZONE);
	}

    public function exec_mpc($cmd) {

        $output = null;

        exec("mpc $cmd ; echo $?", $output);

        $ret_status = array_pop($output);

        if ($ret_status != 0) {
                // /!\ Something went wrong /!\
                return null;
        }

        return $output;
    }

    public function sync_lcd() {

        // we don't need an atomic precision !
        usleep(1000000);

        if ($output = $this->exec_mpc("status")) {

            if (count($output) == 3) {
                // MPD playing
                list($status, $tracker, $timings, $percent) = sscanf($output[1], "%s #%s %s (%d)");

                // index
                $index = explode("/", $tracker);
                // clock
                $clock = explode("/", $timings);
                // \u{00A0}\u{00A0} = unicode &nbsp;
                echo $index[0] . "\u{00A0}\u{00A0}" . $clock[0];
            }
            else {
                // MPD not playing
                echo "-\u{00A0}--:--";
            }
        }
        else{
            echo "ERR";
        }
    }

	public function sync_idle() {

        $t1 = hrtime(true);

        if ($output = $this->exec_mpc("idle")) {
                // MPD triggered new event
                echo "OK";
        }
        else {
            // exec_mpc has returned an error

            $t2 = hrtime(true);

            if (($t2 - $t1)/1000000 < 100) {
                // mpc "idle" cmd returns in less than 100 ms
                // MPD not started ?
                // waiting until "status" returns OK
                while (!$output = $this->exec_mpc("status")) {
                        sleep(1);
                }
                echo "WAKE_UP";
            }
            else {
                // mpc "idle" cmd returns in more than 100 ms
                // MPD killed ?
                echo "KILLED";
            }
        }
    }
}