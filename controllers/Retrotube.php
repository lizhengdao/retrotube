<?php
/**
 * Retrotube V1.3
 * Copyright (c) 2020 eric.yape@gmail.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ------------------------------------------------------------------------------
 * @author    eric.yape@gmail.com
 * @license   GNU GENERAL PUBLIC LICENSE Version 3
 * @link      https://gitlab.com/eric.yape/retrotube
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Retrotube extends CI_Controller {

	public static $select_modes = array (
        'clear-add-play',
        'crop-add-play',
        'add-play'
	);
	
	public static $display_modes = array (
		'library',
		'playlists',
		'cover',
		'artist',
		'albumartist',
		'album',
		'date',
		'genre'
	);

    public static $filter_types = array (
        "artist" => "artist:",
        "albumartist" => "albumartist:",
        "album" => "album:",
        "title" => "title:",
        "track" => "track:",
        "name" => "name:",
        "genre" => "genre:",
        "date" => "date:",
        "composer" => "composer:",
        "performer" => "performer:",
        "comment" => "comment:",
        "disc" => "disc:",
        "filename" => "filename:",
		"any" => "any:"
	);

	public function __construct()
	{
			parent::__construct();
			//
			header('Access-Control-Allow-Origin: *');
			header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
			//
			$this->load->model('Retrotube_model');
			$this->load->helper('url_helper');
			$this->load->helper('form');
			$this->load->library('session');

			date_default_timezone_set(RT_TIMEZONE);
	}

	public function hello() {

		// Code Igniter sanity check !
		echo "Hello World :-) <BR> Happy listening with Retrotube V1.3 !<BR>";
	}

	public function arg_decode($arg) {

		return str_replace("~~", "/", base64_decode(urldecode($arg)));
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function refresh() {

		$data = array();

		$data['current_track'] = $this->Retrotube_model->get_CurrentTrack();

		$data['current_file'] = $this->Retrotube_model->get_CurrentFile();

		$data['current_albums'] = $this->Retrotube_model->get_CurrentAlbums();

		$data['playlist'] = $this->Retrotube_model->get_Playlist();

		$data['covers_suffix'] = $this->Retrotube_model->get_CoversSuffix();

		$data['lcd'] = $this->Retrotube_model->get_Lcd();

		if ($this->session->has_userdata('display_mode')) {

			$display_mode = $this->session->display_mode;
		}
		else {
			$display_mode = 'library';
		}
		$data['display_mode'] = $display_mode;
		$display_mode_lenm1 = strlen($display_mode)-1;

		if ($this->session->has_userdata('select_mode')) {

			$data['select_mode'] = $this->session->select_mode;
		}
		else {
			$data['select_mode'] = 'clear-add-play';
		}

		if (!strcmp($display_mode, 'library')) {

			if ($this->session->has_userdata('filter_type') && $this->session->has_userdata('sort_type')) {
	
				$filter_type = $this->session->filter_type;
				$filter_value = $this->session->filter_value;
				$sort_type = $this->session->sort_type;
				$sort_dir = $this->session->sort_dir;

				$data['covers'] = $this->Retrotube_model->get_SortedSearch($sort_type, $sort_dir, $filter_type, $filter_value);
				
			} else if ($this->session->has_userdata('sort_type')) {
	
				$sort_type = $this->session->sort_type;
				$sort_dir = $this->session->sort_dir;
	
				$data['covers'] = $this->Retrotube_model->get_Sort($sort_type, $sort_dir);
				
			} else if ($this->session->has_userdata('filter_type')) {
	
				$filter_type = $this->session->filter_type;
				$filter_value = $this->session->filter_value;

				$data['covers'] = $this->Retrotube_model->get_Search($filter_type, $filter_value);
				
			} else {
				$data['covers'] = $this->Retrotube_model->get_AllCovers();
			}

			$data['found'] = array();
			$data['playlists'] = array();
		}
		else if (!strcmp($display_mode, 'playlists')) {

			$data['covers'] = array();
			$data['found'] = array();
			$data['playlists'] = $this->Retrotube_model->get_AllPlaylists();
		}
		else if (!strcmp($display_mode, 'cover')) {

			$data['covers'] = array();
			$data['found'] = array();
			$data['playlists'] = array();
		}
		else {
			// display mode is using filters
			$data['covers'] = array();
			$data['found'] = $this->Retrotube_model->get_All( $display_mode );
			$data['playlists'] = array();
		}

		$data['filter_type'] = $this->session->filter_type;
		$data['filter_value'] = $this->session->filter_value;
		$data['sort_type'] = $this->session->sort_type;
		$data['sort_dir'] = $this->session->sort_dir;

		$data['select_modes'] = self::$select_modes;
		$data['display_modes'] = self::$display_modes;
		$data['filter_types'] = self::$filter_types;

		$this->load->view("Retrotube/player", $data);
	}

	private function _display_library() {

		$this->session->set_userdata('display_mode', 'library');

		// CLEAR cookie
		if (isset($_COOKIE['scrollTop']))
			unset($_COOKIE['scrollTop']);

		$this->refresh();
	}

	public function index()
	{
		/*
		// CLEAR filters
		$this->session->unset_userdata('filter_type');
		$this->session->unset_userdata('filter_value');

		// CLEAR sort
		$this->session->unset_userdata('sort_type');
		$this->session->unset_userdata('sort_dir');

		// CLEAR modes
		$this->session->unset_userdata('display_mode');
		$this->session->unset_userdata('select_mode');
		*/
		
		$this->_display_library();
	}

	public function play() {

		$this->Retrotube_model->play();
		
		$this->refresh();
	}

	public function playalbum($album = null) {

		if ($album) {

			$album = $this->arg_decode($album);

			if ($this->session->has_userdata('select_mode')) {

				$select_mode = $this->session->select_mode;

				if (!strcmp($select_mode, 'clear-add-play')) {
					$this->Retrotube_model->clear();
					$this->Retrotube_model->addalbum($album);
					$this->Retrotube_model->play();
				}
				else if (!strcmp($select_mode, 'crop-add-play')) {
					$this->Retrotube_model->crop();
					$this->Retrotube_model->addalbum($album);
					$this->Retrotube_model->play();
				}
				else if (!strcmp($select_mode, 'add-play')) {
					$this->Retrotube_model->addalbum($album);
					$this->Retrotube_model->play();
				}
			}
			else {
				// DEFAULT mode
				$this->Retrotube_model->clear();
				$this->Retrotube_model->addalbum($album);
				$this->Retrotube_model->play();
			}
		}
		else {
			// NOP
		}

		$this->refresh();
	}

	public function playlist($playlist = null) {

		if ($playlist) {

			$playlist = $this->arg_decode($playlist);

			if ($this->session->has_userdata('select_mode')) {

				$select_mode = $this->session->select_mode;

				if (!strcmp($select_mode, 'clear-add-play')) {
					$this->Retrotube_model->clear();
					$this->Retrotube_model->loadplaylist($playlist);
					$this->Retrotube_model->play();
				}
				else if (!strcmp($select_mode, 'crop-add-play')) {
					$this->Retrotube_model->crop();
					$this->Retrotube_model->loadplaylist($playlist);
					$this->Retrotube_model->play();
				}
				else if (!strcmp($select_mode, 'add-play')) {
					$this->Retrotube_model->loadplaylist($playlist);
					$this->Retrotube_model->play();
				}
			}
			else {
				// DEFAULT mode
				$this->Retrotube_model->clear();
				$this->Retrotube_model->loadplaylist($playlist);
				$this->Retrotube_model->play();
			}
		}
		else {
			// NOP
		}

		$this->refresh();
	}

	public function playtrack($trackNumber = null) {

		if ($trackNumber) {

			$this->Retrotube_model->playtrack($trackNumber);
		}
		else {
			// NOP
		}
		
		$this->refresh();
	}

	public function pause() {

		$this->Retrotube_model->pause();

		$this->refresh();
	}

	public function stop() {

		$this->Retrotube_model->stop();
		
		$this->refresh();
	}

	public function prev() {

		$this->Retrotube_model->prev();
		
		$this->refresh();
	}

	public function next() {

		$this->Retrotube_model->next();
		
		$this->refresh();
	}

	public function volup() {

		$this->Retrotube_model->volup();
		
		$this->refresh();
	}

	public function voldown() {

		$this->Retrotube_model->voldown();
		
		$this->refresh();
	}

	public function eject() {

		$this->Retrotube_model->eject();
		
		$this->refresh();
	}

	public function mpc() {

		$mpc_cmd = $this->input->post('mpc_cmd');
		$mpc_args = $this->input->post('mpc_args');

		$this->Retrotube_model->mpc($mpc_cmd, $mpc_args);

		$this->refresh();
	}

	private function _check_display_mode($display_mode) {

		if (is_null($display_mode))
			return 0;
		
		foreach (self::$display_modes as $mode) {
			if (!strcmp($display_mode, $mode))
				return 1;
		}

		return 0;
	}

	private function _check_select_mode($select_mode) {

		if (is_null($select_mode))
			return 0;
		
		foreach (self::$select_modes as $mode) {
			if (!strcmp($select_mode, $mode))
				return 1;
		}

		return 0;
	}

	public function display($display_mode = null) {

		if ($this->_check_display_mode($display_mode)) {

			$this->session->set_userdata('display_mode', $display_mode);
		}
		else {
			// default display mode
			$this->session->set_userdata('display_mode', 'library');
		}

		$this->refresh();
	}

	public function select($select_mode = null) {

		if ($this->_check_select_mode($select_mode)) {

			$this->session->set_userdata('select_mode', $select_mode);
		}
		else {
			// default select mode
			$this->session->set_userdata('select_mode', 'clear-add-play');
		}
		
		$this->refresh();
	}

	public function clearsearch() {

		// CLEAR filters
		$this->session->unset_userdata('filter_type');
		$this->session->unset_userdata('filter_value');
		
		$this->_display_library();
	}

	public function search() {

		$filter_type = $this->input->post('filter-type');
		$filter_value = $this->input->post('filter-value');

		if (strlen($filter_value) == 0) {
			// CLEAR filters
			$this->session->unset_userdata('filter_type');
			$this->session->unset_userdata('filter_value');
		}
		else {
			// NEW filters
			$this->session->set_userdata('filter_type', $filter_type);
			$this->session->set_userdata('filter_value', $filter_value);
		}
		
		$this->_display_library();
	}

	public function clearsort() {

		// CLEAR sort
		$this->session->unset_userdata('sort_type');
		$this->session->unset_userdata('sort_dir');
		
		$this->_display_library();
	}

	public function sort() {

		$sort_type = $this->input->post('sort_type');
		$sort_dir = $this->input->post('sort_dir');

		if (!strcmp($sort_type, 'folders')) {
			// CLEAR sort
			$this->session->unset_userdata('sort_type');
			$this->session->unset_userdata('sort_dir');
		}
		else {
			// NEW sort
			$this->session->set_userdata('sort_type', $sort_type);
			$this->session->set_userdata('sort_dir', $sort_dir);
		}
		
		$this->_display_library();
	}

	public function searchitem($filter_type = null, $filter_value = null) {

		if ($filter_type && $filter_value) {

			$filter_value = $this->arg_decode($filter_value);

			// NEW filters
			$this->session->set_userdata('filter_type', $filter_type);
			$this->session->set_userdata('filter_value', $filter_value);
		}
		else {
			// CLEAR filters
			$this->session->unset_userdata('filter_type');
			$this->session->unset_userdata('filter_value');
		}
		
		$this->_display_library();
	}

    public function sync_lcd() {

		return $this->Retrotube_model->sync_lcd();
    }

	public function sync_idle() {

		return $this->Retrotube_model->sync_idle();
    }
}