# Retrotube

**Retrotube** is a simple web application that enables you to control your music library with a gorgeous user interface ! The main view of **Retrotube** displays all your album covers and you just have to click on a cover to start playing your music !

![Retrotube](./screenshots/retrotube-1.png)

## Release notes

| Versions | Dates | Short descriptions |
| ---------- | ---------- | ---------- |
| V1.0 | November 11 2020 | Initial release |
| V1.0.1 | November 15 2020 | "search artist" button `>` in playlist |
| V1.1 | November 16 2020 | Support of MPD playlists |
| V1.1.1 | November 18 2020 | Track timing in LCD |
| V1.2 | November 22 2020 | New selection modes |
| V1.3 | November 26 2020 | New display modes ; status LED |

## Introduction

There is currently a galaxy of different music players and each of them has a set of features that make it different from the others. As a matter of fact, chosing a music player can be a very tedious task if you don't feel comfortable with the way many music players are organized. It also means that chosing a music player is a very serious task ;-)

The design of **Retrotube** is based on a very simple feature: the albums covers are displayed on the main window and you only have to click on a cover to start playing it. If you like this idea then you may continue reading this page ;-)

However this simple design rationale induces two strong constraints :
1. you must have organized your music library around the "concept" of albums (not playlists)
1. you must have collected a cover file for each of yours albums

If your music library is one single folder with hundreds of thousands music files in it, **Retrotube** may not be the more suitable music player for you !

Furthermore, **Retrotube** is of no help if you only listen music via streaming servers : **Retrotube** has no internet features !

_But_, if you have carefully organized your music library by artists/albums and if you have patiently collected the covers of all your albums then **Retrotube** may be your next preferred music player !

## The Music library architecture

As previously explained **Retrotube** music library preferred organization is the following:

![Retrotube music library](./screenshots/music-library.png)

**Retrotube** is able to use the following file naming conventions for covers:
* cover.jpg
* cover.jpeg
* cover.png
* front.jpg
* front.jpeg
* front.png

If you are using an other file naming convention you may just change the `$covers_suffix` variable in `models/Retrotube_model.php`

Depending on your file system properties the letter case of the cover file name should be taken into account (or not) !

## The software architecture (aka the "Big Picture")

> The install process may be a bit long and confusing. Then you may considered the following diagram as a guide to help you to understand how things glue together.

The development of the **Retrotube** software was only targeted to propose a nice user interface. Therefore, **Retrotube** is using other software modules to achieve a rich and exciting listening experience:
1. the HTML/CSS user interface is rendered with a web browser and a http server
1. the audio files are managed with mpc (Music Player Client) and mpd (Music Player Daemon)
1. the internal logic is implemented with the Code Igniter PHP framework

![Software architecture](./screenshots/software-modules.png)

## Supported platforms

**Retrotube** is still in a development phase and it has been only tested on Arch Linux ! But since Arch Linux is always using latest packages versions it ensures that **Retrotube** is compatible with up-to-date software modules. However if you are using a legacy Linux distro you may encounter unwanted effects (mainly in the HTML/CSS rendering of the user interface)...

> **Retrotube** is still using some OS dependent systems calls. Then it is not yet compatible with OS that do not support **bash** shell syntax (as MS Windows).

## Installation

> Hold your breath. Make a wish ("I want to install Retrotube"). Count to three. And go for it :-)

### Music Player Daemon (mpd) and Client (mpc)

**Retrotube** is using [mpd](https://www.musicpd.org/) and [mpc](https://www.musicpd.org/clients/mpc/) to send music files to your audio output.

To check if `mpd` and `mpc` are corectly installed on your system, type `mpc listall`: it should output all your music files.

### HTTP server

**Retrotube** needs a HTTP server to display the HTML/CSS based user interface. It's currently tested with [Apache](https://httpd.apache.org/). Support of other HTTP servers (nginx, lighttpd) is under investigations...

The `/srv/http/Retrotube` git folder should be configured as a new entry point in your http server.

### Code Igniter PHP framework

**Retrotube** is developped with the [Code Igniter](https://codeigniter.com/) (V3) PHP framework, then first install [PHP](https://www.php.net/) on your system !

**Retrotube** is also using some [php-gd](https://www.php.net/manual/fr/book.image.php) functions : you must check that php-gd is enabled in `php.ini`

For a new Code Igniter installation, unzip the CI V3 files in `/usr/local/CI3` (edit the root folder name for readability) otherwise edit `/srv/http/Retrotube/index.php` (cf below).

Code Igniter V3 is based on the Model-View-Controller design pattern and therefore there is a **Retrotube** PHP file for each role:
1. Model role: copy the `models/Retrotube_model.php` git file into the `/usr/local/CI3/application/models` folder
1. View role: copy the `views/Retrotube/player.php` git file into the `/usr/local/CI3/application/views` folder
1. Controller role: copy the `controllers/Retrotube.php` git file into the `/usr/local/CI3/application/controllers` folder

Three additionnal files should also be verified or changed :
1. `/srv/http/Retrotube/index.php` : verify or change
```
	$application_folder = '/usr/local/CI3/application';
	$system_path = '/usr/local/CI3/system';
```
2. `/usr/local/CI3/application/config/config.php` : verify or change
```
$config['sess_save_path'] = '/srv/http/Retrotube/sessions';
```
3. `/usr/local/CI3/application/config/constants.php` : verify or change
```
define('RT_TIMEZONE', 'UTC');
define('RT_MUSIC_LIBRARY', '/srv/http/Retrotube/MusicLibrary/'); // need trailing slash
```

> `/srv/http/Retrotube` is the folder configured in the http server.\
> `/usr/local/CI3` is the Code Igniter root folder

### The bindfs utility

The [bindfs](https://bindfs.org/) utility is the last thing you may have to configure. As a matter of fact, the MPD Music Directory (configured in your `mpd.conf`) may not be accessible from within the http server because it is owned by a different OS user. The `bindfs` utility enables the http server to access this folder by remapping the `mpd` OS access rights to the http process OS access rights.

Just follow these steps:

A. For the `mpd` process:
1. Record which music folder is used my the `mpd` process (cf `music_directory` entry in `mpd.conf`) as `folder1`
1. Find who is running the `mpd` process : `ps -eaf | grep mpd`
1. Record the first column returned by the previous command as `user1`
1. Find the OS identifiers of `user1` : `id user1`
1. Record the returned user and group identifiers as `id1` and `group1`

B. For the http process:
1. Record the name of the music folder used in the http server (cf `define('RT_MUSIC_LIBRARY', [...])` in `config/config.php`) as `folder2`. Default value of `folder2` should then be `/srv/http/Retrotube/MusicLibrary`
1. `folder2` must be empty before any call to `bindfs` : remove `README.txt` if it is still in the folder !
1. Find who is running the http server process : `ps -eaf | grep httpd` (apache) or `ps -eaf | grep nginx` (nginx)
1. Record the first column returned by the previous command as `user2`
1. Find the OS identifier of `user2` : `id user2`
1. Record the returned user and group identifiers as `id2` and `group2`

C. Bind the two users OS access rights with `bindfs` (use recorded info):
```
sudo bindfs --map=id1/id2:@group1/@group2 folder1 folder2
```

You may check if `bindfs` has correctly map the folders with something like :
```
sudo find /srv/http/Retrotube/MusicLibrary/ -name 'cover.*'
```
You must get all the covers **Retrotube** needs to work.

Et voilà ! ;-) You are now ready to start **Retrotube** (congratulations).

## User manual

### Launch **Retrotube**

Point your web browser to http://myserver/Retrotube/index.php/Retrotube

After few seconds, all your albums should be displayed in the central main window and you should be able to control the mpd server with the left control panel.

### The left Control Panel

The upper part of the left control panel gathers actions on the `mpd` server and the current track:

![The upper part of left Control Panel](./screenshots/control-panel-1.png)

1. An option menu with the available `mpc` commands and a text field for an optional argument. You can select a `mpc` command in the menu, fill an optional text argument and click the `Exec!` button : the selected command is sent to the `mpd` server. You would not use this menu very often since most of the common commands are more conveniently arranged below...
1. A LCD window with the sequence number of the currently played track and the time duration of this track
1. The control buttons, from left to right:
 * Clear the current playlist
 * Volume Up (+5%)
 * Volume Down (-5%)
 * Play track
 * Pause track
 * Stop track
 * Previous track
 * Next track

The lower part of the Control panel gathers actions on the current playlist:

![The upper part of left Control Panel](./screenshots/control-panel-2.png)

1. The artist and album names of the current playlist are shown
2. The list of the tracks in the current playlist. For each track is displayed:
* `#`: the sequence number in the playlist
* `idx`: the index of the track in the album (discnumber.tracknumber)
* `-Title-`: the title of the track
* `>>`: a shortcut to search for the artist's track in the music library
* `time`: the duration of the track
3. The album art cover

To play a specific track, click on its title in the list.

### The main view

The goal of the main view is to quickly access the music files: it enables the user to easily sort and filter the albums and to display the whole artists directory. Plus you can display the current album cover to add a visual artwork to the listening experience :-)

The upper bar of the main view is divided into 3 mini control panels plus a status LED:

**The status LED** (V1.3)

In the left corner of the main view bar, a small green/red LED gives the status of the `Retrofocus` application:
* ![The Available status LED](./screenshots/library-available-1.png) : the `Retrotube` application is available and ready to handle new user commands

* ![The Busy status LED](./screenshots/library-busy-1.png): the `Retrotube` application is busy with some activity (most of the time views refreshing) and can not handle new user commands

**The `sort by` control panel**

![The sort mini control](./screenshots/library-sort-1.png)

The first option menu enables to sort the music library with the following sort keys:
* folders (the default sort key)
* albums
* artists / albums
* artists / dates / albums
* dates / albums
* dates / artists / albums

> The `folders` sort key is the "natural" order of the MPD music_directory folder hierarchy and the sort direction is not usable (yet).

The second option menu enables 3 different sort directions:
* Ascending
* Descending
* None (random !)

The actions:
* Click the `Sort!` button to apply the selected sort
* Click the `<X` button to clear the selected criteria and to refresh the main window

**The `search` control panel**

![The search mini control](./screenshots/library-search-1.png)

The first option menu enables to select a search criteria:
* artist
* albumartist
* album
* title
* track
* name
* genre
* date
* composer
* performer
* comment
* disc
* filename
* any (any criteria in the previous list)

The text field enables you to enter the search value

The actions:
* Click the `Search!` button to launch the search
* Click the `<X` button to clear the search criteria and to refresh the main window

**The `display` control panel**

![The Display mini control](./screenshots/library-display-1.png)

The `display` mini control panel modifies the content of the main view:
* `library`: the albums are displayed (default display mode)
* `playlists` (V1.1): the MPD playlists are displayed. Just click on a playlist to start playing it
* `cover`: the cover of the current album is displayed
* `artist`: the list of tracks artists is displayed. Just click on an artist to start a search in the library with this artist as a search criteria
* `albumartist` (V1.3): the directory of albums artists is displayed. Just click on an album artist to start a search in the library with this album artist as a search criteria
* `date` (V1.3): the list of tracks dates is displayed. Just click on a date to start a search in the library with this date as a search criteria
* `genre` (V1.3): the list of tracks genres is displayed. Just click on a genre to start a search in the library with this genre as a search criteria

The `cover` display mode:
![The Cover display mode](./screenshots/retrotube-cover.jpg)

> A click on the album cover in the left control panel or in the main view toggles `library` and `cover` views.

The `artist` display mode:
![The Artists display mode](./screenshots/retrotube-artists.jpg)

The `playlists` display mode (V1.1):
![The Playlists display mode](./screenshots/retrotube-playlists.jpg)

**The `select` control panel** (V1.2)

![The Select mini control](./screenshots/library-select-1.png)

The `select` mini control panel modifies the way albums are queued on the MPD server when you click on a cover in the `library` view:
* `clear-add-play`: the current playlist is cleared, the selected album content is queued on the MPD server and playing is started
* `crop-add-play`: the current playlist is cropped (all songs are removed except for the currently playing song), the selected album content is queued and playing is started. This mode allows you to add a new album in the MDP queue without music interruption.
* `add-play`: songs of the selected album are added at the end of the current playlist and playing is started

## One more thing...

Since you read this page up to here, you deserve to see a cooool **Retrotube** feature: the dynamic synchronization with the `mpd` server ! As a matter of fact the **Retrotube** UI is automatically refreshed when the state of the `mpd` server is modified. For example, if a `mpd` client modifies the playlist, or jump to the next track, the user interface of **Retrotube** is refreshed to display the new playlist or the new current track. **Retrotube** is also tracking the state of the `mpd` process: when the `mpd` process is killed or started, the UI is also automatically refreshed.

See it in action:\
![Retrotube dynamic sync](./screenshots/retrotube-dyn-sync.mp4)

## Known bugs

* From time to time the album art cover is not correctly positionned in the left control panel: it moves below the bottom line ! Just click on the Play button `(>)` and everything should come back to its right place. ( spend **hours** on it and can't find a fix :-/ )
* With the exception of the previous one: Zarro Boogs !

## Link and Author

https://gitlab.com/eric.yape/retrotube

eric.yape@gmail.com

(work in progress)
