
This folder is used to store Code Igniter sessions data !

It must be referenced in config/config.php:

$config['sess_save_path'] = '/srv/http/Retrotube/sessions';
