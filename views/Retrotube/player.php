<!-- Retrotube V1.3 - https://gitlab.com/eric.yape/retrotube - Copyright (c) 2020 eric.yape@gmail.com -->
<?php
    $href_play      = site_url('Retrotube/play');
    $href_playalbum = site_url('Retrotube/playalbum');
    $href_playtrack = site_url('Retrotube/playtrack');
    $href_playlist  = site_url('Retrotube/playlist');
    $href_pause     = site_url('Retrotube/pause');
    $href_stop      = site_url('Retrotube/stop');
    $href_prev      = site_url('Retrotube/prev');
    $href_next      = site_url('Retrotube/next');
    $href_volup     = site_url('Retrotube/volup');
    $href_voldown   = site_url('Retrotube/voldown');
    $href_eject     = site_url('Retrotube/eject');
    $href_refresh   = site_url('Retrotube/refresh');
    $href_search    = site_url('Retrotube/search');
    $href_searchitem= site_url('Retrotube/searchitem');
    $href_sort      = site_url('Retrotube/sort');
    $href_mpc       = site_url('Retrotube/mpc');
    $href_display   = site_url('Retrotube/display');
    $href_clearsort = site_url('Retrotube/clearsort');
    $href_select    = site_url('Retrotube/select');
    $href_clearsearch = site_url('Retrotube/clearsearch');
    $href_clearselect = site_url('Retrotube/clearselect');
    //
    $href_sync_lcd  = site_url('RetrotubeSync/sync_lcd');
    $href_sync_idle = site_url('RetrotubeSync/sync_idle');
    //
    $href_images    = base_url('images');
    $href_music     = base_url('MusicLibrary');

    // encode controller's arguments
    function arg_encode($arg) {
        return urlencode(base64_encode(str_replace("/", "~~", $arg)));
    }

    // filter_types is given by controller
    function is_filter_type($filter_types, $filter_type) {

        foreach( $filter_types as $value => $display ) {
            if (!strcmp($value, $filter_type))
                return true;
        }
        return false;
    }

    // https://stackoverflow.com/questions/3468500/detect-overall-average-color-of-the-picture
    function getAverageColor($sourceURL) {

        $image = imagecreatefromstring(file_get_contents(RT_MUSIC_LIBRARY . $sourceURL));
        /*
        imagealphablending($image, false); // imagesavealpha can only be used by doing this for some reason
        imagesavealpha($image, true); // this one helps you keep the alpha. 
        imagefilter($image, IMG_FILTER_COLORIZE, 0, 0, 0, 127 * (1.0 - 0.4));
        */
        $scaled = imagescale($image, 3, 3, IMG_BICUBIC); 
        $index = imagecolorat($scaled, 1, 0);
        $rgb = imagecolorsforindex($scaled, $index); 

        return sprintf('#%02X%02X%02X', $rgb['red'], $rgb['green'], $rgb['blue']);
     }

    // https://www.jonasjohn.de/snippets/php/color-inverse.htm
    function getComplementaryColor($color) {

        $color = str_replace('#', '', $color);
        if (strlen($color) != 6){ return '000000'; }
        $rgb = '';
        for ($x=0;$x<3;$x++){
            $c = 255 - hexdec(substr($color,(2*$x),2));
            $c = ($c < 0) ? 0 : dechex($c);
            $rgb .= (strlen($c) < 2) ? '0'.$c : $c;
        }
        return '#'.$rgb;
    }
    
    if (!strcmp($display_mode, "library")) {
        $gallery_display_style = "display: flex;";
        $cover_display_style = "display: block;";
        $raw_viewer_display_style = "display: none;";
        $directory_display_style = "display: none;";
        $playlists_display_style = "display: none;";
    }
    else if (!strcmp($display_mode, "playlists")) {
        $gallery_display_style = "display: none;";
        $cover_display_style = "display: block;";
        $raw_viewer_display_style = "display: none;";
        $directory_display_style = "display: none;";
        $playlists_display_style = "display: flex;";
    }
    else if (!strcmp($display_mode, "cover")) {
        $gallery_display_style = "display: none;";
        $cover_display_style = "display: none;";
        $raw_viewer_display_style = "display: block;";
        $directory_display_style = "display: none;";
        $playlists_display_style = "display: none;";
    }
    else if ( is_filter_type($filter_types, $display_mode) ) {
        // display mode is using filters
        $gallery_display_style = "display: none;";
        $cover_display_style = "display: block;";
        $raw_viewer_display_style = "display: none;";
        $directory_display_style = "display: flex;";
        $playlists_display_style = "display: none;";
    }

    if (array_key_exists('scrollTop', $_COOKIE)) {

        $top = explode(",", $_COOKIE['scrollTop']);
        $gallery_scrollTop = $top[0];
        $index_scrollTop = $top[1];
        $directory_scrollTop = $top[2];

    }
    else {
        $gallery_scrollTop = 0;
        $index_scrollTop = 0;
        $directory_scrollTop = 0;
    }

    function is_selected($option, $selection) {
		return (isset($selection) && !strcmp($option, $selection)) ? "selected" : "";
    }
    function _is_selected($option, $selection) {
		return (isset($selection) && !strcmp($option, $selection)) ? "_selected" : "";
    }

    $sort_types = array (
        "folders" => "folders", // DEFAULT
        "albums" => "albums",
        "artists-albums" => "artists/albums",
        "artists-dates-albums" => "artists/dates/albums",
        "dates-albums" => "dates / albums",
        "dates-artists-albums" => "dates/artists/albums");

    $sort_dirs = array (
        "asc" => "asc",
        "desc" => "desc",
        "none" => "none");

    $select_modes = array (
        "clear-add-play" => "clear-add-play", // DEFAULT
        "crop-add-play" => "crop-add-play",
        "add-play" => "add-play"
    );

    //echo $_COOKIE['scrollTop'];
    //echo "gallery_scrollTop = $gallery_scrollTop ; index_scrollTop = $index_scrollTop ; directory_scrollTop = $directory_scrollTop <BR>";
    //echo $lcd . "<BR>";
?>
<html>
    <link rel="stylesheet" type="text/css" href="<?= base_url('css/Retrotube.css') ?>">
    <script type="text/javascript" src="<?= base_url('js/Retrotube.js') ?>"></script>
    <body>

    <div class="folder">
        <div class="column1" id="#column1">
            <div class="title" id="#title1">Controls
                <form style="display: inline;" action="<?= $href_mpc ?>" method="post" accept-charset="utf-8">
                    <label class="index_key" for="mpc_cmd">  / mpc </label>
                    <select class="index_value" name="mpc_cmd" id="mpc_cmd">
                        <option class="index_key" value="add">add &lt;uri&gt;</option>
                        <option class="index_key" value="cdprev">cdprev</option>
                        <option class="index_key" value="channels">channels</option>
                        <option class="index_key" value="clear">clear</option>
                        <option class="index_key" value="clearerror">clearerror</option>
                        <option class="index_key" value="consume">consume &lt;on|off&gt;</option>
                        <option class="index_key" value="crop">crop</option>
                        <option class="index_key" value="crossfade">crossfade [&lt;seconds&gt;]</option>
                        <option class="index_key" value="current">current</option>
                        <option class="index_key" value="del">del &lt;position&gt;</option>
                        <option class="index_key" value="disable">disable [only] &lt;output # or name&gt; [...]</option>
                        <option class="index_key" value="enable">enable [only] &lt;output # or name&gt; [...]</option>
                        <option class="index_key" value="findadd">findadd &lt;type&gt; &lt;query&gt;</option>
                        <option class="index_key" value="find">find &lt;type&gt; &lt;query&gt;</option>
                        <option class="index_key" value="idle">idle [events]</option>
                        <option class="index_key" value="idleloop">idleloop [events]</option>
                        <option class="index_key" value="insert">insert &lt;uri&gt;</option>
                        <option class="index_key" value="listall">listall [&lt;file&gt;]</option>
                        <option class="index_key" value="list">list &lt;type&gt; [&lt;type&gt; &lt;query&gt;]</option>
                        <option class="index_key" value="listneighbors">listneighbors</option>
                        <option class="index_key" value="load">load &lt;file&gt;</option>
                        <option class="index_key" value="ls">ls [&lt;directory&gt;]</option>
                        <option class="index_key" value="lsplaylists">lsplaylists</option>
                        <option class="index_key" value="mixrampdb">mixrampdb [&lt;dB&gt;]</option>
                        <option class="index_key" value="mixrampdelay">mixrampdelay [&lt;seconds&gt;]</option>
                        <option class="index_key" value="mount">mount [&lt;uri&gt; &lt;storage&gt;]</option>
                        <option class="index_key" value="move">move &lt;from&gt; &lt;to&gt;</option>
                        <option class="index_key" value="next">next</option>
                        <option class="index_key" value="outputset">outputset &lt;output # or name&gt;</option>
                        <option class="index_key" value="outputs">outputs</option>
                        <option class="index_key" value="pause-if-playing">pause-if-playing</option>
                        <option class="index_key" value="pause">pause</option>
                        <option class="index_key" value="playlist">playlist [&lt;playlist&gt;]</option>
                        <option class="index_key" value="play]">play [&lt;position&gt;]</option>
                        <option class="index_key" value="prev">prev</option>
                        <option class="index_key" value="prio">prio &lt;prio&gt; &lt;position/range&gt; ...</option>
                        <option class="index_key" value="queued">queued</option>
                        <option class="index_key" value="random">random &lt;on|off&gt;</option>
                        <option class="index_key" value="repeat">repeat &lt;on|off&gt;</option>
                        <option class="index_key" value="replaygain">replaygain [off|track|album]</option>
                        <option class="index_key" value="rescan">rescan [&lt;path&gt;]</option>
                        <option class="index_key" value="rm">rm &lt;file&gt;</option>
                        <option class="index_key" value="save">save &lt;file&gt;</option>
                        <option class="index_key" value="searchadd">searchadd &lt;type&gt; &lt;query&gt;</option>
                        <option class="index_key" value="searchplay">searchplay &lt;pattern&gt;</option>
                        <option class="index_key" value="search">search &lt;type&gt; &lt;query&gt;</option>
                        <option class="index_key" value="seek">seek [+-][HH:MM:SS]|&lt;0-100&gt;%</option>
                        <option class="index_key" value="seekthrough">seekthrough [+-][HH:MM:SS]</option>
                        <option class="index_key" value="sendmessage">sendmessage &lt;channel&gt; &lt;message&gt;</option>
                        <option class="index_key" value="shuffle">shuffle</option>
                        <option class="index_key" value="single">single &lt;on|off&gt;</option>
                        <option class="index_key" value="stats">stats</option>
                        <option class="index_key" value="sticker">sticker &lt;uri&gt; &lt;cmd&gt; [args..]</option>
                        <option class="index_key" value="stop">stop</option>
                        <option class="index_key" value="subscribe">subscribe &lt;channel&gt;</option>
                        <option class="index_key" value="toggleoutput">toggleoutput &lt;output # or name&gt; [...]</option>
                        <option class="index_key" value="toggle">toggle</option>
                        <option class="index_key" value="unmount">unmount &lt;uri&gt;</option>
                        <option class="index_key" value="update">update [&lt;path&gt;]</option>
                        <option class="index_key" value="version">version</option>
                        <option class="index_key" value="volume">volume [+-]&lt;num&gt;</option>
                        <option class="index_key" value="waitmessage">waitmessage &lt;channel&gt;</option>
                    </select>
                    <input class="index_value" type="text" name="mpc_args" size=16 value="">
                    <input class="player_button" type="submit" value="Exec!">
                </form>
            </div>

            <div class="player" id="#cdplayer">
                <img src="<?= "$href_images/cd-player.png" ?>" width="100%" usemap="#cd-player-map">
                <div class="player_lcd" id="#player_lcd"><?= $lcd ?></div>
                <div class="player_tray" title="<?= substr($current_albums, 2) ?>"><?= substr($current_albums, 2) ?></div>
            </div>
            <!-- div cdplayer -->

            <div class="player_index" id="#index"> 
                <?php
                    // current playlist defined ?
                    if (count($playlist)): ?>
                    <?php
                        $player_cover_url = null;
                        $index_full_style = "";
                        // current track playing ?
                        if (strlen($current_file)) {

                            // extract suffix from current file
                            $current_suffix     = substr($current_file, -2);
                            $current_file       = substr($current_file, 0, strlen($current_file)-2);
                            $current_folder     = dirname($current_file);
                            $current_cover_url  = arg_encode($current_folder);
                            //$current_cover_anchor = "/#cover_$current_cover_url";

                            // Does a cover exist ?
                            if ($current_suffix[1] != '0') {
                                // current track has a cover
                                $current_cover_file = $covers_suffix[ $current_suffix ];
                                $player_cover_file  = "${current_folder}${current_cover_file}";
                                $player_cover_url   = "$href_music/$player_cover_file";

                                if (strcmp($display_mode, 'cover')) {
                                    $index_bg_color     = getAverageColor($player_cover_file);
                                    $index_fg_color     = getComplementaryColor($index_bg_color);
                                    $index_full_style   = "color: $index_fg_color; background-color: ${index_bg_color}80;" ;
                                }
                            }
                        }
                    ?>

                    <?php if (isset($player_cover_url)): ?>
                        <img class="player_index_cover" id="#player_index_cover" style="<?= $cover_display_style ?>" src="<?= $player_cover_url ?>" onclick="player_display('cover')">
                    <?php endif ?>

                    <div class='player_index_playlist'>
                        <?php
                            $multi_artists = $current_albums[0] != '1' ;
                            $multi_albums = $current_albums[1] != '1' ;
                            $num = 1;
                        ?>

                        <table class="index_full" style="<?= $index_full_style ?>">
                            <tr>
                                <th class="index_key player_index_key">#</th>
                                <th class="index_key player_index_key_r">idx</th>
                                <th class="index_key player_index_key">-Title-</th>
                                <th class="index_key player_index_key_l"></th>
                                <th class="index_key player_index_key_l">time</th>
                            </tr>
                            <?php foreach ($playlist as $track): ?>

                                <?php
                                    // from Retrotube_model::$mpc_track_fmt
                                    // %position% ~~ [%disc%.]%track% ~~ %time% ~~ %artist% ~~ %albumartist% ~~ %album% ~~ %title%
                                    // 0             1                   2         3           4                5          6
                                    $infos = explode(' ~~ ', $track, 7);

                                    if ($multi_artists) {
                                        // multi artists playlist
                                        $title = (strlen($infos[4]) > 0) ? $infos[4] . ' :: ' : $infos[3] . ' :: ';
                                    }
                                    else
                                        $title = "";

                                    if ($multi_albums) {
                                        // multi albums playlist
                                        $title .= $infos[5] . ' :: ';
                                    }

                                    if ((strlen($infos[4]) > 0) && strcmp($infos[3], $infos[4])) {
                                        // %albumartist% != %artist%
                                        $title .= $infos[3] . ' :: ';
                                    }

                                    $title .= $infos[6];
                                ?>

                                <tr>
                                    <td class="index_key player_index_key"><?= $infos[0] ?></td>
                                    <td class="index_key player_index_key_r"><?= $infos[1] ?></td>
                                    <td class="player_button<?= _is_selected($track, $current_track) ?> player_track_button" onclick="player_playtrack('<?= $num++ ?>')"><?= $title ?></td>
                                    <td class="player_button player_track_button" onclick="player_searchitem('artist', '<?= arg_encode($infos[3]) ?>')">&gt;</td>
                                    <td class="index_key player_index_key_l"><?= $infos[2] ?></td>
                                </tr>
                            <?php endforeach; ?>

                            <tr>
                                <td></td>
                                <td></td>
                                <td class="index_key player_index_key">-.-</td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                    <!-- div player_index_playlist -->
                <?php endif ?>

            </div>
            <!-- div player_index -->
            <img id="#cdplayerbottom" src="<?= "$href_images/cd-player-bottom.png" ?>" width="100%">
        </div>
        <!-- div column1 -->

        <div class="column2" id="#column2">
            <div class="title" id="#title2">
                <img id="#activity_status" class="activity_status" src="<?= base_url('images/user-busy.png') ?>" width=16 height=16>
                Library
                <form style="display: inline;" action="<?= $href_sort ?>" method="post" accept-charset="utf-8">
                    <label class="index_key" for="sort_type">/sort by</label>
                    <select class="index_value" name="sort_type" id="sort_type">
                        <?php foreach ($sort_types as $value => $display): ?>
                            <option class="index_key" value="<?= $value ?>" <?= is_selected($value, $sort_type) ?>><?= $display ?></option>
                        <?php endforeach ?>
                    </select>
                    <select class="index_value" name="sort_dir" id="sort_dir">
                        <?php foreach ($sort_dirs as $value => $display): ?>
                            <option class="index_key" value="<?= $value ?>" <?= is_selected($value, $sort_dir) ?>><?= $display ?></option>
                        <?php endforeach ?>
                    </select>
                    <input class="player_button" type="submit" value="Sort!">
                </form>
                <div class="player_button" style="display: inline;" onclick="player_clearsort()">&lt;X</div>

                <form style="display: inline;" action="<?= $href_search ?>" method="post" accept-charset="utf-8">
                    <label class="index_key" for="filter-type">/search</label>
                    <select class="index_value" name="filter-type" id="filter-type">
                        <?php foreach ($filter_types as $value => $display): ?>
                            <option class="index_key" value="<?= $value ?>" <?= is_selected($value, $filter_type) ?>><?= $display ?></option>
                        <?php endforeach ?>
                    </select>
                    <input class="index_value" type="text" name="filter-value" value="<?= isset($filter_value) ? $filter_value : "" ?>">
                    <input class="player_button" type="submit" value="Search!">
                </form>
                <div class="player_button" style="display: inline;" onclick="player_clearsearch()">&lt;X</div>

                <label class="index_key">/display</label>
                <?php foreach ($display_modes as $mode): ?>
                    <div class="player_button<?= _is_selected($mode, $display_mode) ?>" style="display: inline;" onclick="player_display('<?= $mode ?>')"><?= $mode ?></div>
                <?php endforeach ?>

                <label class="index_key">/select</label>
                <?php foreach ($select_modes as $mode): ?>
                    <div class="player_button<?= _is_selected($mode, $select_mode) ?>" style="display: inline;" onclick="player_select('<?= $mode ?>')"><?= $mode ?></div>
                <?php endforeach ?>

            </div>

            <div class="gallery" id="#gallery" style="<?= $gallery_display_style ?>">
                <?php foreach ($covers as $cover): ?>
                    <?php
                        $suffix = substr($cover, -2);
                        $cover = substr($cover, 0, strlen($cover)-2);
                        $cover_url = arg_encode($cover);
                        if ($suffix[1] != '0') {
                            // cover found
                            $player_gallery_cdcase = "$href_images/cd-case.png";
                            $cover_file = $covers_suffix[ $suffix ];
                        }
                        else {
                            // no cover
                            $sep = strpos($cover, "/");
                            $artist = substr($cover, 0, $sep);
                            $album = substr($cover, $sep+1);
                            $player_gallery_cdcase = "$href_images/no-cover.png";
                        }
                    ?>

                    <div class="player_gallery" onclick="player_playalbum('<?= $cover_url ?>')" > 
                        <?php if ($suffix[1] != '0'): ?>

                            <img class="player_gallery_cover" src="<?= "$href_music/${cover}${cover_file}" ?>">
                            <img class="player_gallery_cdcase" src="<?= $player_gallery_cdcase ?>" title="<?= $cover ?>">
                        <?php else: ?>

                            <img class="player_gallery_cdcase" src="<?= $player_gallery_cdcase ?>" title="<?= $cover ?>">
                            <div class="player_cover_artist" title="<?= $artist ?>"><?= $artist ?></div>
                            <div class="player_cover_album" title="<?= $album ?>"><?= $album ?></div>
                        <?php endif ?>

                    </div>
                <?php endforeach; ?>

            </div>

            <div class="raw_viewer" id="#raw_viewer" style="<?= $raw_viewer_display_style ?>">
                <?php if (isset($player_cover_url)): ?>

                    <img class="player_full_cover" src="<?= $player_cover_url ?>" onclick="player_display('library')">
                <?php endif ?>

            </div>

            <div class="gallery" id="#directory" style="<?= $directory_display_style ?>">
                <?php foreach($found as $item): ?>

                    <div class="player_button" onclick="player_searchitem('<?= $display_mode ?>', '<?= arg_encode($item) ?>')"><?= $item ?></div>
                <?php endforeach; ?>
                
            </div>

            <div class="gallery" id="#playlists" style="<?= $playlists_display_style ?>">
                <?php foreach($playlists as $playlist): ?>

                    <div class="player_gallery" onclick="player_playlist('<?= arg_encode($playlist) ?>')">
                        <img class="player_gallery_cdcase" src="<?= "$href_images/no-cover.png" ?>" title="<?= $playlist ?>">
                        <div class="player_cover_artist" title="<?= $playlist ?>"><?= $playlist ?></div>
                    </div>

                <?php endforeach; ?>
                
            </div>
        </div>
    </div>
    <map name="cd-player-map">
        <area shape="circle" coords="25,170,14"  href="#" onclick="player_eject()"  alt="Eject" />
        <area shape="circle" coords="87,170,14"  href="#" onclick="player_volup()"  alt="Volume up" />
        <area shape="circle" coords="138,170,14" href="#" onclick="player_voldown()" alt="Volume down" />
        <area shape="circle" coords="201,170,14" href="#" onclick="player_play()"   alt="Play track" />
        <area shape="circle" coords="263,170,14" href="#" onclick="player_pause()"  alt="Pause track" />
        <area shape="circle" coords="314,170,14" href="#" onclick="player_stop()"   alt="Stop track" />
        <area shape="circle" coords="376,170,14" href="#" onclick="player_prev()"   alt="Previous track" />
        <area shape="circle" coords="427,170,14" href="#" onclick="player_next()"   alt="Next track" />
    </map>
    <script type="text/javascript">

        var idle_req = null;
        var scrollTops = { "#gallery": <?= $gallery_scrollTop ?>, "#index": <?= $index_scrollTop ?>, "#directory": <?= $directory_scrollTop ?> };

        function clearViewersScrollTop() {

            document.cookie = "scrollTop=0,0,0; path=/ ; SameSite=Strict ;";
        }

        function getScrollTop(id) {

            var element = document.getElementById(id);
            return (element.style.display == "none") ? scrollTops[id] : element.scrollTop;
        }

        function saveViewersScrollTop() {

            document.cookie = "scrollTop=" + getScrollTop("#gallery") + "," + getScrollTop("#index")  + "," + getScrollTop("#directory") + "; path=/ ; SameSite=Strict ;";
        }

        function setScrollTop(id, value) {

            var element = document.getElementById(id);
            element.scrollTop = value;
        }

        function setViewersScrollTop() {

            setScrollTop("#gallery", <?= $gallery_scrollTop ?>);
            setScrollTop("#index", <?= $index_scrollTop ?>);
            setScrollTop("#directory", <?= $directory_scrollTop ?>);
        }

        function beamMeUpScotty(where) {

            setBusyStatus();
            abort_idle();
            saveViewersScrollTop();
            window.location.href = where;
        }

        function player_allowDrop(event) {

            event.preventDefault();
        }

        function player_drop(event) {

            event.preventDefault();
            // TODO ...
        }

        function player_eject() {

            beamMeUpScotty("<?= $href_eject ?>");
        }

        function player_volup() {

            beamMeUpScotty("<?= $href_volup ?>");
        }

        function player_voldown() {

            beamMeUpScotty("<?= $href_voldown ?>");
        }

        function player_play() {

            beamMeUpScotty("<?= $href_play ?>");
        }

        function player_pause() {

            beamMeUpScotty("<?= $href_pause ?>");
        }

        function player_stop() {

            beamMeUpScotty("<?= $href_stop ?>");
        }

        function player_prev() {

            beamMeUpScotty("<?= $href_prev ?>");
        }

        function player_next() {

            beamMeUpScotty("<?= $href_next ?>");
        }

        function player_playtrack(track) {

            beamMeUpScotty("<?= $href_playtrack ?>" + '/' + track);
        }

        function player_playalbum(album) {

            beamMeUpScotty("<?= $href_playalbum ?>" + '/' + album);
        }

        function player_playlist(playlist) {

            beamMeUpScotty("<?= $href_playlist ?>" + '/' + playlist);
        }

        function player_searchitem(filter_type, filter_value) {

            beamMeUpScotty("<?= $href_searchitem ?>" + '/' + filter_type + '/' + filter_value);
        }

        function player_clearsearch() {

            beamMeUpScotty("<?= $href_clearsearch ?>");
        }

        function player_clearsort() {

            beamMeUpScotty("<?= $href_clearsort ?>");
        }

        function player_display(mode) {

            beamMeUpScotty("<?= $href_display ?>" + '/' + mode);
        }

        function player_select(mode) {

            beamMeUpScotty("<?= $href_select ?>" + '/' + mode);
        }

        function ack_lcd() {

            if (this.readyState === 4 && this.status === 200) {
                response = this.responseText;
                lcd = document.getElementById("#player_lcd");
                lcd.textContent = response;
            }
            sync_lcd();
        }

        function sync_lcd() {

            req = new XMLHttpRequest();
            req.onload = ack_lcd;
            req.open("get", "<?= $href_sync_lcd ?>", true);
            req.send();
        }

        function abort_idle() {

            if (idle_req != null) {
                //idle_req.abort();
                idle_req = null;
            }
        }

        function ack_idle() {

            //response = this.responseText;
            if (this.readyState === 4 && this.status === 200) {
                beamMeUpScotty("<?= $href_refresh ?>");
                //beamMeUpScotty(window.location);
            }
        }

        function sync_idle() {

            idle_req = new XMLHttpRequest();
            idle_req.onload = ack_idle;
            idle_req.open("get", "<?= $href_sync_idle ?>", true);
            idle_req.send();
        }

        function setBusyStatus() {
            var img = document.getElementById("#activity_status");
            img.src = "<?= base_url('images/user-busy.png') ?>";
        }

        function setAvailableStatus() {
            var img = document.getElementById("#activity_status");
            img.src = "<?= base_url('images/user-available.png') ?>";
        }

        setViewersDimension();
        setViewersScrollTop();
        //
        sync_idle();
        sync_lcd();
        //
        window.onresize = setViewersDimension;
        window.onload = setAvailableStatus;

    </script>
    </body>
</html>