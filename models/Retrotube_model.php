<?php
/**
 * Retrotube V1.3
 * Copyright (c) 2020 eric.yape@gmail.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ------------------------------------------------------------------------------
 * @author    eric.yape@gmail.com
 * @license   GNU GENERAL PUBLIC LICENSE Version 3
 * @link      https://gitlab.com/eric.yape/retrofocus
 */

class Retrotube_model extends CI_Model {

       // Code Igniter does not provide 'real' static members
       // https://stackoverflow.com/questions/38474632/codeigniter-using-static-variable
       public static $mpc_track_fmt = "%position% ~~ [%disc%.]%track% ~~ %time% ~~ %artist% ~~ %albumartist% ~~ %album% ~~ %title%";

       public static $covers_suffix = array(
              ':1'   =>     "/cover.jpg",
              ':2'   =>     "/cover.jpeg",
              ':3'   =>     "/cover.png",
              ':4'   =>     "/front.jpg",
              ':5'   =>     "/front.jpeg",
              ':6'   =>     "/front.png"
       );

       public function __construct()
       {
              parent::__construct();
              //echo "new Retrotube_model\n";
       }

       public function get_CoversSuffix() {

              return self::$covers_suffix;
       }

       public function has_cover($mpd_folder) {

              foreach ($this->get_CoversSuffix() as $suffix => $cover_file) {

                     if (is_file(RT_MUSIC_LIBRARY . $mpd_folder . $cover_file)) {
                            return $suffix;
                     }
              }

              if (is_dir(RT_MUSIC_LIBRARY . $mpd_folder)) {
                     return ":0";
              }
              
              return null;
       }

       public function esc_mpc_arg($arg) {

              return str_replace('"', '\\"', $arg);
       }

       public function exec_mpc($cmd) {

              $output = null;

              exec("mpc $cmd ; echo $?", $output);

              $ret_status = array_pop($output);

              if ($ret_status != 0) {
                     // /!\ Something went wrong /!\
                     return null;
              }

              return $output;
       }

       public function get_AllCovers()
       {
              $covers = array();

              if ($output = $this->exec_mpc("ls")) {

                     foreach ($output as $folder) {
       
                            $albums = null;
       
                            $folder = $this->esc_mpc_arg($folder);
       
                            $albums = $this->exec_mpc("ls \"$folder\"");

                            if ($albums) {
                                   // Music directory folder
                                   foreach ($albums as $album) {
              
                                          $suffix = $this->has_cover($album);
                                          if (!is_null($suffix))
                                                 $covers[] = $album . $suffix;
                                   }
                            }
                            else {
                                   // playlist : ignore !
                            }
                     }
              }

              return $covers;
       }

       public function get_All($tag)
       {
              if ($output = $this->exec_mpc("list $tag | sort"))
                     return $output;
              else
                     return array();
		
       }

       public function get_AllArtists()
       {
              return $this->get_All("artist");
		
       }

       public function get_AllAlbumArtists()
       {
              return $this->get_All("albumartist");
		
       }

       public function get_AllDates()
       {
              return $this->get_All("date");
		
       }

       public function get_AllGenres()
       {
              return $this->get_All("genre");
		
       }

       public function get_AllAlbums()
       {
              return $this->get_All("album");
		
       }

       public function get_AllPlaylists() {

              if ($playlists = $this->exec_mpc("lsplaylists | sort"))
                     return $playlists;
              else
                     return array();

       }

       public function get_SortedSearch($sort_type, $sort_dir, $filter_type, $filter_value)
       {
              //echo "get_SortedSearch $sort_type $sort_dir $filter_type $filter_value <BR>" ;

              if (!strcmp($sort_type, "folders")) {

                     return $this->get_Search($filter_type, $filter_value);
              }
              
              $mpc_sort_fmt = "";

              if (!strcmp($sort_dir, "desc")) {
                     $sort_opt = " -r";
              }
              else if (!strcmp($sort_dir, "none")) {
                     $sort_opt = " -R";
              }
              else {
                     $sort_opt = "";
              }

              if (!strcmp($sort_type, "albums")) {

                     $mpc_sort_fmt = "%album%";
              }
              else if (!strcmp($sort_type, "artists-albums")) {

                     $mpc_sort_fmt = "[%albumartist%|%artist%] %album%";
              }
              else if (!strcmp($sort_type, "artists-dates-albums")) {

                     $mpc_sort_fmt = "[%albumartist%|%artist%] %date% %album%";
              }
              else if (!strcmp($sort_type, "dates-albums")) {

                     $mpc_sort_fmt = "%date% %album%";
              }
              else if (!strcmp($sort_type, "dates-artists-albums")) {

                     $mpc_sort_fmt = "%date% [%albumartist%|%artist%] %album%";
              }
              else {
                     // /!\ unknown sort_type
                     $mpc_sort_fmt = "%album%";
              }

		$filter_value = $this->esc_mpc_arg($filter_value);

              $covers = array();

              if ($output = $this->exec_mpc("-f '" . $mpc_sort_fmt . " ~~ %file%' search $filter_type \"$filter_value\" | sort $sort_opt")) {
              
                     foreach ($output as $line) {
       
                            $sep = strpos($line, "~~");
                            $file = substr($line, $sep+3);
                            $album = dirname($file);
       
                            $suffix = $this->has_cover($album);
                            if (!is_null($suffix) && !in_array($album . $suffix, $covers))
                                   $covers[] = $album . $suffix;
                     }
              }
              
              return $covers;
       }

       public function get_Search($filter_type, $filter_value)
       {
              $filter_value = $this->esc_mpc_arg($filter_value);
              
              $covers = array();

              if ($output = $this->exec_mpc("search $filter_type \"$filter_value\"")) {
              
                     foreach ($output as $track) {
       
                            $album = dirname($track);
       
                            $suffix = $this->has_cover($album);
                            if (!is_null($suffix) && !in_array($album . $suffix, $covers))
                                   $covers[] = $album . $suffix;
                     }
              }
              
              return $covers;
       }

       public function get_Sort($sort_type, $sort_dir)
       {
              if (!strcmp($sort_type, "folders")) {

                     // default sort
                     return $this->get_AllCovers();
              }

              $mpc_sort_fmt = "";

              if (!strcmp($sort_dir, "desc")) {
                     $sort_opt = " -r";
              }
              else if (!strcmp($sort_dir, "none")) {
                     $sort_opt = " -R";
              }
              else {
                     $sort_opt = "";
              }

              if (!strcmp($sort_type, "albums")) {

                     $mpc_sort_fmt = "%album%";
              }
              else if (!strcmp($sort_type, "artists-albums")) {

                     $mpc_sort_fmt = "[%albumartist%|%artist%] %album%";
              }
              else if (!strcmp($sort_type, "artists-dates-albums")) {

                     $mpc_sort_fmt = "[%albumartist%|%artist%] %date% %album%";
              }
              else if (!strcmp($sort_type, "dates-albums")) {

                     $mpc_sort_fmt = "%date% %album%";
              }
              else if (!strcmp($sort_type, "dates-artists-albums")) {

                     $mpc_sort_fmt = "%date% [%albumartist%|%artist%] %album%";
              }
              else {
                     // unknown sort type !
                     $mpc_sort_fmt = "%album%";
              }

              $covers = array();

              if ($output = $this->exec_mpc("-f '" . $mpc_sort_fmt . " ~~ %file%' listall | sort $sort_opt")) {
                     
                     foreach ($output as $line) {
       
                            $sep = strpos($line, "~~");
                            $track = substr($line, $sep+3);
                            $album = dirname($track);
       
                            $suffix = $this->has_cover($album);
                            if (!is_null($suffix) && !in_array($album . $suffix, $covers))
                                   $covers[] = $album . $suffix;
                     }
              }

              return $covers;
       }

       public function get_CurrentFile()
       {
		if ($output = $this->exec_mpc("-f '%file%' current")) {
              
                     if (count($output) > 0) {
                            $file = $output[0];
                            $folder = dirname($file);
                            $suffix = $this->has_cover($folder);
       
                            return $file . $suffix;
                     }
              }
              
              return null;
       }

       public function get_CurrentTrack()
       {
		if ($output = $this->exec_mpc("-f '" . self::$mpc_track_fmt . "' current")) {

                     if(count($output) > 0)
                            return $output[0];
              }
              
              return null;
       }

       // TODO : remove duplicate function sync_lcd()
       public function get_Lcd()
       {
              if ($output = $this->exec_mpc("status")) {

                     if(count($output) == 3) {
                            // MPD playing
                            list($status, $tracker, $timings, $percent) = sscanf($output[1], "%s #%s %s (%d)");

                            // index
                            $index = explode("/", $tracker);
                            // clock
                            $clock = explode("/", $timings);
                            // \u{00A0}\u{00A0} = unicode &nbsp;
                            return $index[0] . "\u{00A0}\u{00A0}" . $clock[0];
                     }
                     else {
                            // MPD not playing
                            return "-\u{00A0}--:--";
                     }
              }
              
              return "ERR";
       }

       public function get_Playlist()
       {
              if ($output = $this->exec_mpc("-f '" . self::$mpc_track_fmt . "' playlist"))
                     return $output;
              else
                     return array();
       }

       public function get_CurrentAlbums()
       {
              $albums = array();
              $artists = array();
              $playlist = array();
              if ($output = $this->exec_mpc("-f '[%albumartist%|%artist%] :: %album%' playlist")) {
              
                     foreach ($output as $line) {

                            if (! in_array($line, $playlist)) {
                                   //count distinct info
                                   $playlist[] = $line;

                                   // count distinct %albumartist%|%artist% and %album%
                                   $infos = explode(' :: ', $line, 2);
                                   if (!in_array($infos[0], $artists)) {
                                          $artists[] = $infos[0];
                                   }
                                   if (!in_array($infos[1], $albums)) {
                                          $albums[] = $infos[1];
                                   }
                            }
                     }
              }

              $nb_artists = count($artists) > 1 ? '+' : '1';
              $nb_albums = count($albums) > 1 ? '+' : '1';
              
              $n = 0;
              $currentAlbums = "(no disc)";
              foreach ($playlist as $line) {
                     if ($n) {
                            $currentAlbums .= " | $line";
                     }
                     else
                            $currentAlbums = $line;
                     $n++;
              }

              return "${nb_artists}${nb_albums}$currentAlbums";
       }

	public function play() {

		$this->exec_mpc("play");
	}

	public function clear() {

		$this->exec_mpc("clear");
	}

	public function crop() {

		$this->exec_mpc("crop");
	}

	public function addalbum($album) {
              
              $album = $this->esc_mpc_arg($album);

		// Set the album content as playlist
		$this->exec_mpc("add \"$album\"");
	}

	public function loadplaylist($playlist) {
              
              $playlist = $this->esc_mpc_arg($playlist);

		// Set the album content as playlist
		$this->exec_mpc("load \"$playlist\"");
	}

	public function playtrack($trackNumber) {

		$this->exec_mpc("play $trackNumber");
	}

	public function pause() {

		$this->exec_mpc("pause");
	}

	public function stop() {

		$this->exec_mpc("stop");
	}

	public function prev() {

              // V1.0
              // $this->exec_mpc("prev");

              // V1.1
		$this->exec_mpc("cdprev");
	}

	public function next() {

		$this->exec_mpc("next");
	}

	public function volup() {

		$this->exec_mpc("volume +5");
	}

	public function voldown() {

		$this->exec_mpc("volume -5");
	}

	public function eject() {

		$this->clear();
	}

	public function mpc($mpc_cmd, $mpc_args) {

		if (strlen($mpc_args) > 0) {

                     $mpc_args = $this->esc_mpc_arg($mpc_args);
                     
			$this->exec_mpc("$mpc_cmd \"$mpc_args\"");
		}
		else {
			$this->exec_mpc("$mpc_cmd");
		}
	}

       // TODO : sync functions are now in RetrotubeSync controller
       public function sync_lcd() {
   
           // we don't need an atomic precision !
           usleep(1000000);
   
           if ($output = $this->exec_mpc("status")) {
   
               if (count($output) == 3) {
                   // MPD playing
                   list($status, $tracker, $timings, $percent) = sscanf($output[1], "%s #%s %s (%d)");
   
                   // index
                   $index = explode("/", $tracker);
                   // clock
                   $clock = explode("/", $timings);
                   // \u{00A0}\u{00A0} = unicode &nbsp;
                   echo $index[0] . "\u{00A0}\u{00A0}" . $clock[0];
               }
               else {
                   // MPD not playing
                   echo "-\u{00A0}--:--";
               }
           }
           else{
               echo "ERR";
           }
       }
   
       // TODO : sync functions are now in RetrotubeSync controller
       public function sync_idle() {
   
           $t1 = hrtime(true);
   
           if ($output = $this->exec_mpc("idle")) {
                   // MPD triggered new event
                   echo "OK";
           }
           else {
               // exec_mpc has returned an error
   
               $t2 = hrtime(true);
   
               if (($t2 - $t1)/1000000 < 100) {
                   // mpc "idle" cmd returns in less than 100 ms
                   // MPD not started ?
                   // waiting until "status" returns OK
                   while (!$output = $this->exec_mpc("status")) {
                           sleep(1);
                   }
                   echo "WAKE_UP";
               }
               else {
                   // mpc "idle" cmd returns in more than 100 ms
                   // MPD killed ?
                   echo "KILLED";
               }
           }
       }
}
?>